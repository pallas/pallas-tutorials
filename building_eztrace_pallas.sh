#!/bin/bash


base_dir=$PWD

# install pallas
git clone https://gitlab.inria.fr/pallas/pallas.git
cd pallas
mkdir build install
cd build
export PALLAS_ROOT=$PWD/../install
cmake .. -DCMAKE_INSTALL_PREFIX=$PALLAS_ROOT
make -j 14 && make install

export PATH=$PALLAS_ROOT/bin:$PATH

## Building EZTrace
cd "$base_dir"
git clone https://gitlab.com/eztrace/eztrace.git
cd eztrace
mkdir build install
cd build
export EZTRACE_ROOT=$PWD/../install
cmake .. -DCMAKE_INSTALL_PREFIX="$EZTRACE_ROOT" -DCMAKE_BUILD_TYPE=RelWithDebInfo \
      -DOTF2_ROOT=$PALLAS_ROOT
make -j 14 && make install

export PATH=EZTRACE_ROOT/bin:$PATH
