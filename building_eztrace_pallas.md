# Building EZTrace + Pallas

Pallas is a trace recording library. It can be used by EZTrace, a
tracing tool for parallel applications.

## Building Pallas+EZTrace: one-liner

Use [building_eztrace_pallas.sh](building_eztrace_pallas.sh) to
install Pallas and EZTrace with one simple command line:

```
bash building_eztrace_pallas.sh
```

Then, add the following lines to your `~/.bashrc` (**please replace `PATH_TO_PALLAS` and `PATH_TO_EZTRACE` with the actual path of Pallas and EZTrace**)
```
export PALLAS_ROOT=PATH_TO_PALLAS/install
export EZTRACE_ROOT=PATH_TO_EZTRACE/install
export PATH=$PALLAS_ROOT/bin:$EZTRACE_ROOT/bin:$PATH
```


## Building Pallas

Another possibility is to install Pallas manually. You need to have ZSTD and JSONCPP installed on your machine:
```sh
sudo apt install libzstd-dev libzstd1 libjsoncpp25 libjsoncpp-dev
```

```
git clone https://gitlab.inria.fr/pallas/pallas.git
cd pallas
mkdir build install
cd build
export PALLAS_ROOT=$PWD/../install
cmake .. -DCMAKE_INSTALL_PREFIX=$PALLAS_ROOT
make -j 14 && make install
```


Once installed, you may want to set a few environment variables in your `~/.bashrc`:
```
PALLAS_ROOT=PATH_TO_PALLAS_INSTALL_DIR
export PATH=$PALLAS_ROOT/bin:$PATH
```



## Building EZTrace
You can also install EZTrace manually:

```
git clone https://gitlab.com/eztrace/eztrace.git
cd eztrace
mkdir build install
cd build
export EZTRACE_ROOT=$PWD/../install
cmake .. -DCMAKE_INSTALL_PREFIX="$EZTRACE_ROOT" -DCMAKE_BUILD_TYPE=RelWithDebInfo -DOTF2_ROOT=$PALLAS_ROOT -DEZTRACE_ENABLE_MPI=ON -DEZTRACE_ENABLE_PTHREAD=ON -DEZTRACE_ENABLE_MEMORY=ON
make -j 14 && make install
``` 

Once installed, you may want to set a few environment variables in your `~/.bashrc`:
```
EZTRACE_ROOT=PATH_TO_EZTRACE_INSTALL_DIR
export PATH=EZTRACE_ROOT/bin:$PATH
```

You can test the installation by executing:
```sh
eztrace_avail
```
