# Pallas tutorials


- [Building EZTrace + Pallas](building_eztrace_pallas.md)
- [Creating a trace with EZTrace + Pallas](using_eztrace_pallas.md)

## Additional documentation

- [Pallas documentation](https://gitlab.inria.fr/pallas/pallas)
- [EZTrace documentation](https://gitlab.com/eztrace/eztrace#Documentation)
- [EZTrace tutorials](https://gitlab.com/eztrace/eztrace-tutorials)

