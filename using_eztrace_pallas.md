# Using EZTrace + Pallas

## Simple test programs

If installed with the [Building EZTrace+Pallas
tutorial](building_eztrace_pallas.md), you should find a few test
programs in `eztrace/test/pthread`.


You can generate a new trace with:

```
$ cd eztrace/test/pthread
$ make dummy_thread
$ eztrace -t pthread ./dummy_thread
[...]
[thread #1] loop 8
[thread #0] loop 9
End of thread #0
[thread #1] loop 9
End of thread #1
Warning in OTF2_GlobalDefWriter_WriteClockProperties (/home/trahay/Soft/opt/pallas/libraries/otf2/src/OTF2_GlobalDefWriter.c:23): Not implemented yet
[P0T0] Stopping EZTrace (pid:38959)...

$ pallas_print  dummy_thread_trace/eztrace_log.pallas 
Reading archive dummy_thread_trace/eztrace_log_0.pallas
            Timestamp   Event
          0.000000000   P#0T#0  THREAD_BEGIN()
          0.000000000   P#0T#1  THREAD_BEGIN()
          0.000000000   P#0T#2  THREAD_BEGIN()
          0.000006354   P#0T#2  Enter 14 (sem_post) { sem <1>: 94558551081120}
[...]
          1.953076520   P#0T#2  Leave 8 (pthread_mutex_unlock)
          2.003087167   P#0T#2  Enter 14 (sem_post) { sem <1>: 94558551081152}
          2.003088353   P#0T#2  Leave 14 (sem_post)
          2.003093512   P#0T#2  THREAD_END()
          2.016464867   P#0T#0  Leave 25 (pthread_join)
          2.016491740   P#0T#0  THREAD_END()
```



